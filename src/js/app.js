"use strict"

import * as usefulFunctions from "./components/functions.js"; // Полезные функции
import maskInput from './forms/input-mask.js'; // Маска ввода для форм
import Navigation from './components/navigation.js';  // Мобильное меню
import tabs from './components/tabs.js'; // Tabs
import Spoilers from "./components/spoilers.js";
import { Fancybox } from "@fancyapps/ui"; // Fancybox modal gallery
import {WebpMachine} from "webp-hero"
import * as bootstrap from 'bootstrap';


// IE Webp Support
const webpMachine = new WebpMachine();
webpMachine.polyfillDocument();

// Проверка поддержки webp
usefulFunctions.isWebp();

// Добавление класса после загрузки страницы
usefulFunctions.addLoadedClass();

// Добавление класса touch для для мобильных
usefulFunctions.addTouchClass()

// Mobile 100vh
usefulFunctions.fullVHfix();

// Install input filters.
if (document.querySelector('[data-input]')) {
    usefulFunctions.setInputFilter(document.querySelector('[data-input]'), function(value) {
        return /^-?\d*$/.test(value); }, "Mus  t be an integer");
}
// Вкладки (tabs)
tabs();

// Маска для ввода номера телефона
maskInput('input[name="phone"]', '+7 (___) ___-__-__');

// Главное меню
Navigation();

// Spoilers
Spoilers();

// Modal Fancybox
Fancybox.bind('[data-fancybox]', {
    autoFocus: false
})

// Custom Scroll
// Добавляем к блоку атрибут data-simplebar
// Также можно инициализировать следующим кодом, применяя настройки

if (document.querySelectorAll('[data-simplebar]').length) {
	document.querySelectorAll('[data-simplebar]').forEach(scrollBlock => {
		new SimpleBar(scrollBlock, {
			autoHide: false
		});
	});
}

// Color switcher

(() => {

    document.addEventListener('click', (event) => {
        if (event.target.closest('[data-lamination-color]')) {
            const lamination = event.target.closest('[data-lamination]')
            const laminationArray = lamination.querySelectorAll('[data-lamination-color]')
            const laminationItem = event.target.closest('[data-lamination-color]')
            const laminationMedia = lamination.querySelector('[data-lamination-media]')
            let laminationName = laminationItem.dataset.laminationColor
            let laminationImage = laminationItem.dataset.laminationImage
            let laminationWebp = laminationImage.replace(/(.*)\.[^.]+$/, "$1") + '.webp'

            laminationArray.forEach(elem => {
                elem.classList.remove('active');
            });
            laminationItem.classList.add('active')

            laminationMedia.querySelector('img').src = laminationImage
            laminationMedia.querySelector('source').srcset = laminationWebp

            console.log(laminationMedia);
            console.log(laminationName);
            console.log(laminationImage);
            console.log(laminationWebp);
        }
    });
})();

/* Calculator */
import "./components/_calc.js";

// Sliders
import "./components/sliders.js";
