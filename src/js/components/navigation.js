export default () => {

    document.addEventListener('click',  (event) => {
        if(event.target.closest('[data-nav-toggle]')) {
            document.querySelector('body').classList.toggle('nav-open')
            return false
        }
        else if (event.target.closest('[data-nav-parent]')) {
            const windowInnerWidth = document.documentElement.clientWidth

            if (windowInnerWidth < 1600) {
                const navGroup = event.target.closest('[data-nav-group]')
                const navChildren = navGroup.querySelector('[data-nav-children]')
                navGroup.classList.toggle('open')

                if (navGroup.classList.contains('open')) {
                    navChildren.style.maxHeight = navChildren.scrollHeight + 'px';
                } else {
                    navChildren.style.maxHeight = null;
                }
            }
        }
    })


    const navParents = document.querySelectorAll('.navigation__group')
    navParents.forEach(elem => {
        elem.onmouseenter = function(event) {
            if (this != event.currentTarget) { return false; }
            const windowInnerWidth = document.documentElement.clientWidth
            if (windowInnerWidth > 1200) {
                document.querySelector('body').classList.add('navigation-open')
            }
        }
        elem.onmouseleave = function(event) {
            if (this != event.currentTarget) { return false; }
            const windowInnerWidth = document.documentElement.clientWidth
            if (windowInnerWidth > 1200) {
                document.querySelector('body').classList.remove('navigation-open')
            }
        }
    });




    /*
    navParents.addEventListener('mouseenter', (event) => {
        if(event.target.closest('.navigation__group')){

        }
    });

    navParents.addEventListener('mouseleave', (event) => {
        if(event.target.closest('.navigation__group')){

        }
    });

     */

};
