import wNumb from "wnumb";
import * as noUiSlider from 'nouislider';
import * as usefulFunctions from "./functions.js";

// Документация плагина: https://refreshless.com/nouislider/

export function calc() {

    if (document.querySelector('[data-range-height]')) {

        const rangeHeight = document.querySelector('[data-range-height]')
        const rangeHeightStart = rangeHeight.getAttribute('data-start').split(",").map(parseFloat);
        const rangeHeightMin = parseFloat(rangeHeight.getAttribute('data-min'));
        const rangeHeightMax = parseFloat(rangeHeight.getAttribute('data-max'));
        const rangeHeightOrientation = rangeHeight.getAttribute('data-orientation');
        const inputHeight = document.querySelector('[data-input-height]')

        noUiSlider.create(rangeHeight, {
            start: rangeHeightStart,
            step: 1,
            tooltips: [ wNumb({ decimals: 0 })],
            orientation: rangeHeightOrientation,
            range: {
                'min': rangeHeightMin,
                'max': rangeHeightMax,
            }
        });

        rangeHeight.noUiSlider.on('update', function (values, handle) {
            if (handle) {
                inputHeight.value = Math.ceil(values[handle])
            } else {
                inputHeight.value = Math.ceil(values[handle])
            }
        });

        inputHeight.addEventListener('change', function () {
            rangeHeight.noUiSlider.set([this.value, null]);
        });

        usefulFunctions.setInputFilter(inputHeight, function(value) {
            return /^\d*$/.test(value); }, 'Укажите целое число');
    }

    if (document.querySelector('[data-range-width]')) {

        const rangeWidth = document.querySelector('[data-range-width]')
        const rangeWidthStart = rangeWidth.getAttribute('data-start').split(",").map(parseFloat);
        const rangeWidthMin = parseFloat(rangeWidth.getAttribute('data-min'));
        const rangeWidthMax = parseFloat(rangeWidth.getAttribute('data-max'));
        const rangeWidthOrientation = rangeWidth.getAttribute('data-orientation');
        const inputWidth = document.querySelector('[data-input-width]')

        noUiSlider.create(rangeWidth, {
            start: rangeWidthStart,
            step: 1,
            tooltips: [ wNumb({ decimals: 0 })],
            orientation: rangeWidthOrientation,
            range: {
                'min': rangeWidthMin,
                'max': rangeWidthMax,
            }
        });

        rangeWidth.noUiSlider.on('update', function (values, handle) {
            if (handle) {
                inputWidth.value = Math.ceil(values[handle])
            } else {
                inputWidth.value = Math.ceil(values[handle])
            }
        });

        inputWidth.addEventListener('change', function () {
            rangeWidth.noUiSlider.set([this.value, null]);
        });

        usefulFunctions.setInputFilter(inputWidth, function(value) {
            return /^\d*$/.test(value); }, 'Укажите целое число');
    }

    const dataRanges = document.querySelectorAll('[data-range]');
    if (dataRanges.length > 0) {
        dataRanges.forEach(el => {
            let rangeStart = el.getAttribute('data-start').split(",").map(parseFloat);
            let rangeMin = parseFloat(el.getAttribute('data-min'));
            let rangeMax = parseFloat(el.getAttribute('data-max'));
            let rangeOrientation = el.getAttribute('data-orientation');

            noUiSlider.create(el, {
                start: rangeStart,
                step: 1,
                tooltips: [ wNumb({ decimals: 0 })],
                orientation: rangeOrientation,
                range: {
                    'min': rangeMin,
                    'max': rangeMax,
                }
            });
        });
    }

    document.addEventListener('click', (event) => {
        if(event.target.closest('[data-calc-type]')) {

            const itemArray = document.querySelectorAll('[data-calc-type]')
            const item = event.target.closest('[data-calc-type]')

            itemArray.forEach(elem => {
                elem.classList.remove('active');
            });
            item.classList.add('active');
        }
    })

    document.addEventListener('click', (event) => {
        if(event.target.closest('[data-window]')) {
            const windows = event.target.closest('[data-windows]')
            const win = event.target.closest('[data-window]')
            const windowArray = windows.querySelectorAll('[data-window]')
            const windowType = win.dataset.window
            const windowMedia = win.querySelector('.item-window__image').innerHTML
            const winThumb = document.querySelector(`[data-calc-type="${windowType}"]`)



            console.log(windowType);

            windowArray.forEach(elem => {
                elem.classList.remove('active');
            });

            win.classList.add('active');
            winThumb.querySelector('i').innerHTML =  windowMedia
            document.querySelector('[data-calc-image]').innerHTML = windowMedia

            modalClose(event)
        }
    })

    function modalClose(event) {
        let modal = event.target.closest('.modal')
        modal.querySelector('[data-bs-dismiss]').click()
    }
}
calc();


