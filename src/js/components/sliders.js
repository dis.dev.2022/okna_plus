/*
Документация по работе в шаблоне:
Документация слайдера: https://swiperjs.com/
Сниппет(HTML): swiper
*/

import Swiper, { Autoplay, Navigation, Pagination, Grid, EffectFade } from 'swiper';

// Инициализация слайдеров
function initSliders() {
    // Перечень слайдеров

    if (document.querySelector('[data-docs]')) {

        new Swiper('[data-docs]', {
            modules: [Navigation],
            slidesPerView: 2,
            spaceBetween: 10,
            navigation: {
                nextEl: '[data-docs-next]',
                prevEl: '[data-docs-prev]',
            },
        });
    }

    if (document.querySelector('[data-person]')) {

        new Swiper('[data-person]', {
            modules: [Navigation],
            slidesPerView: 2,
            spaceBetween: 10,
            navigation: {
                nextEl: '[data-person-next]',
                prevEl: '[data-person-prev]',
            },
        });
    }

    if (document.querySelector('[data-comments]')) {

        new Swiper('[data-comments]', {
            modules: [Navigation, Pagination, Grid],
            slidesPerView: 1,
            grid: {
                rows: 2,
            },
            spaceBetween: 0,
            breakpoints: {
                576: {
                    slidesPerView: 2,
                    spaceBetween: 0,
                    grid: {
                        rows: 1,
                    },
                },
                768: {
                    slidesPerView: 2,
                    spaceBetween: 0,
                    grid: {
                        rows: 1,
                    },
                },
                1200: {
                    slidesPerView: 3,
                    spaceBetween: 0,
                    grid: {
                        rows: 1,
                    },
                },
            },
            navigation: {
                nextEl: '[data-comment-next]',
                prevEl: '[data-comment-prev]',
            },
        });
    }

    if (document.querySelector('[data-videos]')) {

        new Swiper('[data-videos]', {
            modules: [Navigation, Pagination, Grid],
            slidesPerView: 1,
            grid: {
                rows: 2,
            },
            spaceBetween: 0,
            breakpoints: {
                576: {
                    slidesPerView: 2,
                    spaceBetween: 0,
                    grid: {
                        rows: 1,
                    },
                },
                768: {
                    slidesPerView: 2,
                    spaceBetween: 0,
                    grid: {
                        rows: 1,
                    },
                },
                1200: {
                    slidesPerView: 3,
                    spaceBetween: 0,
                    grid: {
                        rows: 1,
                    },
                },
            },
            navigation: {
                nextEl: '[data-videos-next]',
                prevEl: '[data-videos-prev]',
            },
        });
    }

    if (document.querySelector('[data-gallery]')) {

        new Swiper('[data-gallery]', {
            modules: [Navigation, Pagination, Grid],
            slidesPerView: 1,
            grid: {
                rows: 4,
            },
            spaceBetween: 20,
            breakpoints: {
                576: {
                    slidesPerView: 2,
                    spaceBetween: 20,
                    grid: {
                        rows: 2,
                    },
                },
                768: {
                    slidesPerView: 2,
                    spaceBetween: 30,
                    grid: {
                        rows: 2,
                    },
                },
                992: {
                    slidesPerView: 3,
                    spaceBetween: 30,
                    grid: {
                        rows: 2,
                    },
                },
                1200: {
                    slidesPerView: 4,
                    spaceBetween: 30,
                    grid: {
                        rows: 3,
                    },
                },
            },
            navigation: {
                nextEl: '[data-gallery-next]',
                prevEl: '[data-gallery-prev]',
            },
        });
    }

    if (document.querySelector('[data-gallery]')) {

        new Swiper('[data-gallery]', {
            modules: [Navigation, Pagination, Grid],
            slidesPerView: 1,
            grid: {
                rows: 4,
            },
            spaceBetween: 20,
            breakpoints: {
                576: {
                    slidesPerView: 2,
                    spaceBetween: 20,
                    grid: {
                        rows: 2,
                    },
                },
                768: {
                    slidesPerView: 2,
                    spaceBetween: 30,
                    grid: {
                        rows: 2,
                    },
                },
                992: {
                    slidesPerView: 3,
                    spaceBetween: 30,
                    grid: {
                        rows: 2,
                    },
                },
                1200: {
                    slidesPerView: 4,
                    spaceBetween: 30,
                    grid: {
                        rows: 3,
                    },
                },
            },
            navigation: {
                nextEl: '[data-gallery-next]',
                prevEl: '[data-gallery-prev]',
            },
        });
    }

    if (document.querySelector('[data-glazing]')) {

        new Swiper('[data-glazing]', {
            modules: [Navigation],
            slidesPerView: 1,
            spaceBetween: 14,
            breakpoints: {
                576: {
                    slidesPerView: 2,
                    spaceBetween: 14,
                },
                768: {
                    slidesPerView: 2,
                    spaceBetween: 30,
                },
                992: {
                    slidesPerView: 3,
                    spaceBetween: 30,
                },
                1200: {
                    slidesPerView: 3,
                    spaceBetween: 30,
                },
            },
            navigation: {
                nextEl: '[data-glazing-next]',
                prevEl: '[data-glazing-prev]',
            },
        });
    }

    if (document.querySelector('[data-selection]')) {

        new Swiper('[data-selection]', {
            modules: [Navigation, Pagination, Grid],
            slidesPerView: 1,
            slidesPerGroup: 1,
            spaceBetween: 0,
            grid: {
                rows: 3,
            },
            breakpoints: {
                576: {
                    slidesPerView: 1,
                    spaceBetween: 0,
                },
                768: {
                    grid: {
                        rows: 1,
                    },
                    slidesPerView: 2,
                    spaceBetween: 0,
                    slidesPerGroup: 2,
                },
                992: {
                    grid: {
                        rows: 1,
                    },
                    slidesPerView: 3,
                    spaceBetween: 0,
                    slidesPerGroup: 3,
                },
                1200: {
                    grid: {
                        rows: 1,
                    },
                    slidesPerView: 3,
                    slidesPerGroup: 3,
                    spaceBetween: 0,
                },
            },
            navigation: {
                nextEl: '[data-selection-next]',
                prevEl: '[data-selection-prev]',
            },
        });
    }

    if (document.querySelector('[data-internal]')) {

        new Swiper('[data-internal]', {
            modules: [Navigation],
            slidesPerView: 1,
            spaceBetween: 0,
            breakpoints: {
                576: {
                    slidesPerView: 2,
                },
                768: {
                    slidesPerView: 2,
                },
                992: {
                    slidesPerView: 4,
                },
            },
            navigation: {
                nextEl: '[data-internal-next]',
                prevEl: '[data-internal-prev]',
            },
        });
    }

    if (document.querySelector('[data-manufacture]')) {

        new Swiper('[data-manufacture]', {
            modules: [Navigation],
            slidesPerView: 1,
            spaceBetween: 30,
            breakpoints: {
                576: {
                    slidesPerView: 2,
                },
                768: {
                    slidesPerView: 2,
                },
                992: {
                    slidesPerView: 3,
                },
            },
            navigation: {
                nextEl: '[data-manufacture-next]',
                prevEl: '[data-manufacture-prev]',
            },
        });
    }

    if (document.querySelector('[data-balcony]')) {

        const balconyButtons = document.querySelectorAll('[data-balcony-button]')

        const balconySlider =  new Swiper('[data-balcony]', {
            modules: [Navigation],
            slidesPerView: 1,
            spaceBetween: 30,
            initialSlide: 1,
            navigation: {
                nextEl: '[data-balcony-next]',
                prevEl: '[data-balcony-prev]',
            },
        });

        balconySlider.on('slideChange', function () {
              balconyButtons.forEach(elem => {
                elem.classList.remove('active');
            });
            document.querySelector(`[data-balcony-button="${balconySlider.realIndex}"]`).classList.add('active');
        });

        document.addEventListener('click', (event) => {
            if (event.target.closest('[data-balcony-button]')) {
                const button = event.target.closest('[data-balcony-button]')
                const slideNum = parseInt(button.dataset.balconyButton)

                balconyButtons.forEach(elem => {
                    elem.classList.remove('active');
                });
                button.classList.add('active')
                balconySlider.slideTo(slideNum, 400)
            }
        });
    }

    if (document.querySelector('[data-price-slider]')) {

        const priceButtons = document.querySelectorAll('[data-price-nav]')

        const priceSlider = new Swiper('[data-price-slider]', {
            slidesPerView: 1,
            spaceBetween: 0,
        });

        priceSlider.on('slideChange', function () {
            priceButtons.forEach(elem => {
                elem.classList.remove('active');
            });
            document.querySelector(`[data-price-nav="${priceSlider.realIndex}"]`).classList.add('active');
        });

        document.addEventListener('click', (event) => {
            if (event.target.closest('[data-price-nav]')) {
                const button = event.target.closest('[data-price-nav]')
                const slideNum = parseInt(button.dataset.priceNav)

                priceButtons.forEach(elem => {
                    elem.classList.remove('active');
                });
                button.classList.add('active')
                priceSlider.slideTo(slideNum, 400)
            }
        });
    }

}

window.addEventListener("load", function (e) {
    // Запуск инициализации слайдеров
    initSliders();
});
